package br.edu.devmedia.jdbc.gui;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JTabbedPane;

import br.edu.devmedia.jdbc.Exception.NegocioException;
import br.edu.devmedia.jdbc.bo.EstadoBO;
import br.edu.devmedia.jdbc.bo.PessoaBO;
import br.edu.devmedia.jdbc.dto.EnderecoDTO;
import br.edu.devmedia.jdbc.dto.EstadoDTO;
import br.edu.devmedia.jdbc.dto.PessoaDTO;
import br.edu.devmedia.jdbc.util.MensagensUtil;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.SwingConstants;
import java.awt.Component;
import javax.swing.JRadioButton;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.awt.event.ActionEvent;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.JScrollPane;
import javax.swing.border.TitledBorder;
import javax.swing.JInternalFrame;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import javax.swing.UIManager;
import java.awt.Color;

public class Main extends JFrame {

	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JTextField tfNome;
	private JTextField tfCPF;
	private final ButtonGroup buttonGroup = new ButtonGroup();
	private JTextField tfNascimento;
	private DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyy");
	private JTable tableListagem;
	private JTextField tfCpfConsulta;
	private JTable tableConsulta;
	private DefaultTableModel valuesConsulta;
	private final ButtonGroup btgOrderBy = new ButtonGroup();
	private List<Integer> idsPessoas = new ArrayList<Integer>();
	private JTextField tfNomeEdit;
	private JTextField tfCpfEdit;
	private JTextField tfDtNascEdit;
	private final ButtonGroup btgEdit = new ButtonGroup();
	private JRadioButton rbtMascEdit;
	private JRadioButton rbtFemiEdit;
	private JLabel lbIDvalue;
	private JTextField tfLogradouro;
	private JTextField tfNumero;
	private JTextField tfBairro;
	private JTextField tfCidade;
	private JTextField tfCep;
	private JTextField tfLograEdit;
	private JTextField tfNumEdit;
	private JTextField tfBairroEdit;
	private JTextField tfCidadeEdit;
	private JTextField tfCepEdit;
	private PessoaDTO pessoaDTO;
	@SuppressWarnings("rawtypes")
	private JComboBox comboUFEdit;
	private JTextField tfCepConsulta;
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Main frame = new Main();
					frame.setLocationRelativeTo(null);
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public Main() {
		setTitle("Principal");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 768, 553);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		
		JTabbedPane mainTabbedPane = new JTabbedPane(JTabbedPane.TOP);
		GroupLayout gl_contentPane = new GroupLayout(contentPane);
		gl_contentPane.setHorizontalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addComponent(mainTabbedPane, GroupLayout.PREFERRED_SIZE, 742, Short.MAX_VALUE)
		);
		gl_contentPane.setVerticalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addComponent(mainTabbedPane, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
		);
		
		JPanel panelCadastro = new JPanel();
		mainTabbedPane.addTab("Cadastro", null, panelCadastro, null);
		
		JButton btLimpar = new JButton("Limpar");
		btLimpar.setMnemonic('L');
		
		JButton btCadastrar = new JButton("Cadastrar");
		btCadastrar.setMnemonic('C');
		
		JPanel panelCadPrincipal = new JPanel();
		panelCadPrincipal.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Pessoa", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
		
		JPanel panel_2 = new JPanel();
		panel_2.setBorder(new TitledBorder(null, "Sexo", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		
		JRadioButton rdbtnFeminino = new JRadioButton("Feminino");
		buttonGroup.add(rdbtnFeminino);
		
		JRadioButton rdbtnMasculino = new JRadioButton("Masculino");
		buttonGroup.add(rdbtnMasculino);
		GroupLayout gl_panel_2 = new GroupLayout(panel_2);
		gl_panel_2.setHorizontalGroup(
			gl_panel_2.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel_2.createSequentialGroup()
					.addGap(48)
					.addComponent(rdbtnMasculino, GroupLayout.PREFERRED_SIZE, 87, GroupLayout.PREFERRED_SIZE)
					.addGap(18)
					.addComponent(rdbtnFeminino, GroupLayout.PREFERRED_SIZE, 82, GroupLayout.PREFERRED_SIZE)
					.addContainerGap(114, Short.MAX_VALUE))
		);
		gl_panel_2.setVerticalGroup(
			gl_panel_2.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel_2.createSequentialGroup()
					.addContainerGap()
					.addGroup(gl_panel_2.createParallelGroup(Alignment.BASELINE)
						.addComponent(rdbtnFeminino)
						.addComponent(rdbtnMasculino))
					.addContainerGap(19, Short.MAX_VALUE))
		);
		gl_panel_2.linkSize(SwingConstants.HORIZONTAL, new Component[] {rdbtnFeminino, rdbtnMasculino});
		panel_2.setLayout(gl_panel_2);
		
		JPanel panelCadEndereco = new JPanel();
		panelCadEndereco.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Endere\u00E7o", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
		
		tfLogradouro = new JTextField();
		tfLogradouro.setColumns(10);
		
		JLabel lblLogradouro = new JLabel("Logradouro:");
		
		JLabel lblNumero = new JLabel("Numero:");
		
		tfNumero = new JTextField();
		tfNumero.setColumns(10);
		
		JLabel lblBairro = new JLabel("Bairro:");
		
		tfBairro = new JTextField();
		tfBairro.setColumns(10);
		
		JLabel lblCidade = new JLabel("Cidade:");
		
		tfCidade = new JTextField();
		tfCidade.setColumns(10);
		
		JComboBox comboEstados = new JComboBox();
		
		JLabel lblUf = new JLabel("UF:");
		
		tfCep = new JTextField();
		tfCep.setColumns(10);
		
		JLabel lblCep = new JLabel("Cep:");
		GroupLayout gl_panelCadEndereco = new GroupLayout(panelCadEndereco);
		gl_panelCadEndereco.setHorizontalGroup(
			gl_panelCadEndereco.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panelCadEndereco.createSequentialGroup()
					.addContainerGap()
					.addGroup(gl_panelCadEndereco.createParallelGroup(Alignment.LEADING)
						.addComponent(lblLogradouro, GroupLayout.PREFERRED_SIZE, 80, GroupLayout.PREFERRED_SIZE)
						.addComponent(lblNumero, GroupLayout.PREFERRED_SIZE, 65, GroupLayout.PREFERRED_SIZE)
						.addComponent(lblBairro, GroupLayout.PREFERRED_SIZE, 65, GroupLayout.PREFERRED_SIZE)
						.addComponent(lblCidade, GroupLayout.PREFERRED_SIZE, 65, GroupLayout.PREFERRED_SIZE)
						.addComponent(lblCep, GroupLayout.PREFERRED_SIZE, 65, GroupLayout.PREFERRED_SIZE)
						.addComponent(lblUf, GroupLayout.PREFERRED_SIZE, 65, GroupLayout.PREFERRED_SIZE))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(gl_panelCadEndereco.createParallelGroup(Alignment.LEADING)
						.addComponent(tfLogradouro, GroupLayout.PREFERRED_SIZE, 222, GroupLayout.PREFERRED_SIZE)
						.addComponent(tfNumero, GroupLayout.PREFERRED_SIZE, 222, GroupLayout.PREFERRED_SIZE)
						.addComponent(tfBairro, GroupLayout.PREFERRED_SIZE, 222, GroupLayout.PREFERRED_SIZE)
						.addComponent(tfCidade, GroupLayout.PREFERRED_SIZE, 222, GroupLayout.PREFERRED_SIZE)
						.addComponent(tfCep, GroupLayout.PREFERRED_SIZE, 222, GroupLayout.PREFERRED_SIZE)
						.addComponent(comboEstados, GroupLayout.PREFERRED_SIZE, 112, GroupLayout.PREFERRED_SIZE))
					.addContainerGap(50, Short.MAX_VALUE))
		);
		gl_panelCadEndereco.setVerticalGroup(
			gl_panelCadEndereco.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panelCadEndereco.createSequentialGroup()
					.addGroup(gl_panelCadEndereco.createParallelGroup(Alignment.LEADING)
						.addComponent(tfLogradouro, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addGroup(gl_panelCadEndereco.createSequentialGroup()
							.addGap(3)
							.addComponent(lblLogradouro)))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(gl_panelCadEndereco.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_panelCadEndereco.createSequentialGroup()
							.addGap(3)
							.addComponent(lblNumero))
						.addComponent(tfNumero, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(gl_panelCadEndereco.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_panelCadEndereco.createSequentialGroup()
							.addGap(3)
							.addComponent(lblBairro))
						.addComponent(tfBairro, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(gl_panelCadEndereco.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_panelCadEndereco.createSequentialGroup()
							.addGap(3)
							.addComponent(lblCidade))
						.addComponent(tfCidade, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(gl_panelCadEndereco.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_panelCadEndereco.createSequentialGroup()
							.addGap(3)
							.addComponent(lblCep))
						.addComponent(tfCep, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(gl_panelCadEndereco.createParallelGroup(Alignment.TRAILING)
						.addComponent(lblUf)
						.addComponent(comboEstados, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addContainerGap(52, Short.MAX_VALUE))
		);
		gl_panelCadEndereco.linkSize(SwingConstants.HORIZONTAL, new Component[] {lblLogradouro, lblNumero, lblBairro, lblCidade, lblUf, lblCep});
		gl_panelCadEndereco.linkSize(SwingConstants.HORIZONTAL, new Component[] {tfLogradouro, tfNumero, tfBairro, tfCidade, comboEstados, tfCep});
		panelCadEndereco.setLayout(gl_panelCadEndereco);
		
		GroupLayout gl_panelCadastro = new GroupLayout(panelCadastro);
		gl_panelCadastro.setHorizontalGroup(
			gl_panelCadastro.createParallelGroup(Alignment.LEADING)
				.addGroup(Alignment.TRAILING, gl_panelCadastro.createSequentialGroup()
					.addGroup(gl_panelCadastro.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_panelCadastro.createSequentialGroup()
							.addGap(10)
							.addComponent(panelCadPrincipal, GroupLayout.DEFAULT_SIZE, 323, Short.MAX_VALUE))
						.addGroup(gl_panelCadastro.createSequentialGroup()
							.addContainerGap()
							.addComponent(panel_2, GroupLayout.DEFAULT_SIZE, 268, Short.MAX_VALUE)))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(panelCadEndereco, GroupLayout.PREFERRED_SIZE, 390, GroupLayout.PREFERRED_SIZE)
					.addGap(260))
				.addGroup(gl_panelCadastro.createSequentialGroup()
					.addContainerGap()
					.addComponent(btLimpar)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(btCadastrar, GroupLayout.PREFERRED_SIZE, 107, GroupLayout.PREFERRED_SIZE)
					.addContainerGap(759, Short.MAX_VALUE))
		);
		gl_panelCadastro.setVerticalGroup(
			gl_panelCadastro.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panelCadastro.createSequentialGroup()
					.addContainerGap()
					.addGroup(gl_panelCadastro.createParallelGroup(Alignment.BASELINE)
						.addGroup(gl_panelCadastro.createSequentialGroup()
							.addComponent(panelCadPrincipal, GroupLayout.PREFERRED_SIZE, 106, GroupLayout.PREFERRED_SIZE)
							.addGap(4)
							.addComponent(panel_2, GroupLayout.PREFERRED_SIZE, 82, GroupLayout.PREFERRED_SIZE))
						.addComponent(panelCadEndereco, GroupLayout.PREFERRED_SIZE, 192, GroupLayout.PREFERRED_SIZE))
					.addPreferredGap(ComponentPlacement.RELATED, 240, Short.MAX_VALUE)
					.addGroup(gl_panelCadastro.createParallelGroup(Alignment.BASELINE)
						.addComponent(btCadastrar)
						.addComponent(btLimpar))
					.addContainerGap())
		);
		gl_panelCadastro.linkSize(SwingConstants.HORIZONTAL, new Component[] {btLimpar, btCadastrar});
		
		JLabel lblNome = new JLabel("Nome:");
		
		tfNome = new JTextField();
		tfNome.setColumns(10);
		
		tfCPF = new JTextField();
		tfCPF.setColumns(10);
		
		JLabel lblCPF = new JLabel("CPF:");
		
		tfNascimento = new JTextField();
		tfNascimento.setColumns(10);
		
		JLabel lblDtNasc = new JLabel("Dt. Nasc:");
		
		btLimpar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				tfNome.setText("");
				tfCPF.setText("");
				rdbtnMasculino.setSelected(true);
				tfNascimento.setText("");
				tfLogradouro.setText("");
				tfBairro.setText("");
				tfCidade.setText("");
				tfNumero.setText("");
				tfCep.setText("");
			}
		});
		
		btCadastrar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				PessoaDTO pessoaDTO = new PessoaDTO();
				EnderecoDTO enderecoDTO = new EnderecoDTO();
				PessoaBO pessoaBO = new PessoaBO();
				try {
					String nome = tfNome.getText();
					String cpf = tfCPF.getText();
					String dtNasc = tfNascimento.getText();
					char sexo = rdbtnMasculino.isSelected() ? 'M' : 'F';
					
					pessoaBO.validaNome(nome);
					pessoaBO.validaCpf(cpf);
					pessoaBO.validaDtNasc(dtNasc);
					
					pessoaDTO.setNome(nome);
					pessoaDTO.setCpf(Long.parseLong(cpf));
					pessoaDTO.setDt_nasc(dateFormat.parse(dtNasc));
					pessoaDTO.setSexo(sexo);
					
					enderecoDTO.setLogradouro(tfLogradouro.getText());
					enderecoDTO.setBairro(tfBairro.getText());
					enderecoDTO.setCidade(tfCidade.getText());
					enderecoDTO.setCep(tfCep.getText().equals("") ? null : Integer.parseInt(tfCep.getText()));
					enderecoDTO.setNumero(tfNumero.getText().equals("") ? null : Long.parseLong(tfNumero.getText()));
					
					pessoaBO.validaEndereco(enderecoDTO);
					
					pessoaDTO.setEnderecoDTO(enderecoDTO);
					
					EstadoDTO estadoDTO = new EstadoDTO();
					estadoDTO.setId(comboEstados.getSelectedIndex() + 1);
					enderecoDTO.setEstadoDTO(estadoDTO);

					pessoaBO.cadastrar(pessoaDTO);
					MensagensUtil.addMsg(Main.this, "Cadastro efetuado com sucesso!");
					Main.this.dispose();
					main(null);
				} catch (Exception e2) {
					MensagensUtil.addMsg(Main.this, e2.getMessage());
				}
				
			}
		});
		EstadoBO estadoBO = new EstadoBO();
		try {
			String[] listaEstados = convertEstados(estadoBO.listEstados());
			comboEstados.setModel(new DefaultComboBoxModel(listaEstados));
		} catch (Exception e) {
			MensagensUtil.addMsg(Main.this, "Erro ao buscar lista de estados!");
		}
		
		GroupLayout gl_panelCadPrincipal = new GroupLayout(panelCadPrincipal);
		gl_panelCadPrincipal.setHorizontalGroup(
			gl_panelCadPrincipal.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panelCadPrincipal.createSequentialGroup()
					.addContainerGap()
					.addGroup(gl_panelCadPrincipal.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_panelCadPrincipal.createSequentialGroup()
							.addGroup(gl_panelCadPrincipal.createParallelGroup(Alignment.LEADING)
								.addComponent(lblNome, GroupLayout.PREFERRED_SIZE, 65, GroupLayout.PREFERRED_SIZE)
								.addComponent(lblCPF, GroupLayout.PREFERRED_SIZE, 65, GroupLayout.PREFERRED_SIZE))
							.addGap(4)
							.addGroup(gl_panelCadPrincipal.createParallelGroup(Alignment.LEADING)
								.addComponent(tfCPF, GroupLayout.PREFERRED_SIZE, 182, GroupLayout.PREFERRED_SIZE)
								.addComponent(tfNome, GroupLayout.PREFERRED_SIZE, 222, GroupLayout.PREFERRED_SIZE)))
						.addGroup(gl_panelCadPrincipal.createSequentialGroup()
							.addComponent(lblDtNasc, GroupLayout.PREFERRED_SIZE, 65, GroupLayout.PREFERRED_SIZE)
							.addGap(4)
							.addComponent(tfNascimento, GroupLayout.PREFERRED_SIZE, 182, GroupLayout.PREFERRED_SIZE)))
					.addContainerGap(463, Short.MAX_VALUE))
		);
		gl_panelCadPrincipal.setVerticalGroup(
			gl_panelCadPrincipal.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panelCadPrincipal.createSequentialGroup()
					.addGroup(gl_panelCadPrincipal.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_panelCadPrincipal.createSequentialGroup()
							.addGap(3)
							.addComponent(lblNome))
						.addComponent(tfNome, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(gl_panelCadPrincipal.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_panelCadPrincipal.createSequentialGroup()
							.addGap(3)
							.addComponent(lblCPF))
						.addComponent(tfCPF, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(gl_panelCadPrincipal.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_panelCadPrincipal.createSequentialGroup()
							.addGap(3)
							.addComponent(lblDtNasc))
						.addComponent(tfNascimento, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addContainerGap(120, Short.MAX_VALUE))
		);
		gl_panelCadPrincipal.linkSize(SwingConstants.HORIZONTAL, new Component[] {tfNome, tfCPF, tfNascimento});
		panelCadPrincipal.setLayout(gl_panelCadPrincipal);
		panelCadastro.setLayout(gl_panelCadastro);
		
		JPanel panelListagem = new JPanel();
		mainTabbedPane.addTab("Listagem", null, panelListagem, null);
		PessoaBO pessoaBO = new PessoaBO();
		
		JScrollPane scrollPane = new JScrollPane();
		
		JButton btnDeletarTudo = new JButton("Deletar tudo");
		btnDeletarTudo.setMnemonic('D');
		btnDeletarTudo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int resp = JOptionPane.showConfirmDialog(Main.this, "Deseja deletar tudo?");
				if (resp == 0) {
					if (pessoaBO.removeTudo()) {
						MensagensUtil.addMsg(Main.this, "Todos os registros deletados!");
					} else {
						MensagensUtil.addMsg(Main.this, "Erro ao deletar todos os registros!");
					}
				}
			}
		});
		
		JInternalFrame internalFrame = new JInternalFrame("Editar Cadastro");
		internalFrame.setVisible(false);
		
		GroupLayout gl_panelListagem = new GroupLayout(panelListagem);
		gl_panelListagem.setHorizontalGroup(
			gl_panelListagem.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panelListagem.createSequentialGroup()
					.addContainerGap()
					.addGroup(gl_panelListagem.createParallelGroup(Alignment.LEADING, false)
						.addComponent(btnDeletarTudo)
						.addComponent(scrollPane, GroupLayout.DEFAULT_SIZE, 715, Short.MAX_VALUE)
						.addComponent(internalFrame, 0, 0, Short.MAX_VALUE))
					.addContainerGap(264, Short.MAX_VALUE))
		);
		gl_panelListagem.setVerticalGroup(
			gl_panelListagem.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panelListagem.createSequentialGroup()
					.addContainerGap()
					.addComponent(btnDeletarTudo)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(scrollPane, GroupLayout.PREFERRED_SIZE, 114, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(internalFrame, GroupLayout.DEFAULT_SIZE, 306, Short.MAX_VALUE)
					.addContainerGap())
		);
		
		JPanel panel_1 = new JPanel();
		panel_1.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Editar Pessoa", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
		GroupLayout groupLayout = new GroupLayout(internalFrame.getContentPane());
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap()
					.addComponent(panel_1, GroupLayout.PREFERRED_SIZE, 679, Short.MAX_VALUE)
					.addContainerGap())
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap()
					.addComponent(panel_1, GroupLayout.DEFAULT_SIZE, 255, Short.MAX_VALUE)
					.addContainerGap())
		);
		
		tfNomeEdit = new JTextField();
		tfNomeEdit.setColumns(10);
		
		JLabel lbNomeEdit = new JLabel("Nome:");
		
		JLabel lbCPFEdit = new JLabel("CPF:");
		
		tfCpfEdit = new JTextField();
		tfCpfEdit.setColumns(10);
		
		JLabel lbSexoEdit = new JLabel("Sexo:");
		
		tfDtNascEdit = new JTextField();
		tfDtNascEdit.setColumns(10);
		
		JLabel lbDtNascEdit = new JLabel("Dt. Nasc:");
		
		JButton btnSalvar = new JButton("Salvar");
		btnSalvar.setMnemonic('S');
		btnSalvar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				PessoaDTO pessoaDTO = new PessoaDTO();
				EnderecoDTO enderecoDTO = new EnderecoDTO();
				try {
					String nome = tfNomeEdit.getText();
					String cpf = tfCpfEdit.getText();
					String dtNasc = tfDtNascEdit.getText();
					char sexo = rbtMascEdit.isSelected() ? 'M' : 'F';
					
					pessoaBO.validaNome(nome);
					pessoaBO.validaCpf(cpf);
					pessoaBO.validaDtNasc(dtNasc);
					
					pessoaDTO.setId(Integer.parseInt(lbIDvalue.getText()));
					pessoaDTO.setNome(nome);
					pessoaDTO.setCpf(Long.parseLong(cpf));
					pessoaDTO.setDt_nasc(dateFormat.parse(dtNasc));
					pessoaDTO.setSexo(sexo);
					
					enderecoDTO.setId(getPessoaDTO().getEnderecoDTO().getId());
					enderecoDTO.setLogradouro(tfLograEdit.getText());
					enderecoDTO.setBairro(tfBairroEdit.getText());
					enderecoDTO.setCidade(tfCidadeEdit.getText());
					enderecoDTO.setCep(tfCepEdit.getText().equals("") ? null : Integer.parseInt(tfCepEdit.getText()));
					enderecoDTO.setNumero(tfNumEdit.getText().equals("") ? null : Long.parseLong(tfNumEdit.getText()));
					
					pessoaBO.validaEndereco(enderecoDTO);
					
					pessoaDTO.setEnderecoDTO(enderecoDTO);
					
					EstadoDTO estadoDTO = new EstadoDTO();
					estadoDTO.setId(comboUFEdit.getSelectedIndex() + 1);
					enderecoDTO.setEstadoDTO(estadoDTO);

					pessoaBO.atualizar(pessoaDTO);
					MensagensUtil.addMsg(Main.this, "Cadastro atualizado com sucesso!");
					Main.this.dispose();
					main(null);
				} catch (Exception e2) {
					e2.printStackTrace();
					MensagensUtil.addMsg(Main.this, e2.getMessage());
				}
			}
		});
		
		rbtMascEdit = new JRadioButton("Masculino");
		btgEdit.add(rbtMascEdit);
		
		rbtFemiEdit = new JRadioButton("Feminino");
		btgEdit.add(rbtFemiEdit);
		
		JLabel lbId = new JLabel("ID:");
		
		lbIDvalue = new JLabel("");
		
		JPanel panelEndEdit = new JPanel();
		panelEndEdit.setBorder(new TitledBorder(null, "Editar Endere\u00E7o", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		GroupLayout gl_panel_1 = new GroupLayout(panel_1);
		gl_panel_1.setHorizontalGroup(
			gl_panel_1.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel_1.createSequentialGroup()
					.addGroup(gl_panel_1.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_panel_1.createSequentialGroup()
							.addContainerGap()
							.addComponent(panelEndEdit, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
							.addPreferredGap(ComponentPlacement.RELATED, 40, Short.MAX_VALUE)
							.addComponent(btnSalvar, GroupLayout.PREFERRED_SIZE, 71, GroupLayout.PREFERRED_SIZE))
						.addGroup(gl_panel_1.createSequentialGroup()
							.addGroup(gl_panel_1.createParallelGroup(Alignment.LEADING)
								.addGroup(gl_panel_1.createSequentialGroup()
									.addContainerGap()
									.addGroup(gl_panel_1.createParallelGroup(Alignment.LEADING)
										.addComponent(lbNomeEdit, GroupLayout.PREFERRED_SIZE, 65, GroupLayout.PREFERRED_SIZE)
										.addComponent(lbCPFEdit, GroupLayout.PREFERRED_SIZE, 65, GroupLayout.PREFERRED_SIZE))
									.addPreferredGap(ComponentPlacement.RELATED)
									.addGroup(gl_panel_1.createParallelGroup(Alignment.LEADING)
										.addComponent(tfCpfEdit, GroupLayout.PREFERRED_SIZE, 206, GroupLayout.PREFERRED_SIZE)
										.addComponent(tfNomeEdit, GroupLayout.PREFERRED_SIZE, 209, GroupLayout.PREFERRED_SIZE)))
								.addGroup(gl_panel_1.createSequentialGroup()
									.addGap(81)
									.addComponent(lbIDvalue, GroupLayout.PREFERRED_SIZE, 65, GroupLayout.PREFERRED_SIZE))
								.addGroup(gl_panel_1.createSequentialGroup()
									.addContainerGap()
									.addComponent(lbId, GroupLayout.PREFERRED_SIZE, 65, GroupLayout.PREFERRED_SIZE)))
							.addPreferredGap(ComponentPlacement.UNRELATED)
							.addGroup(gl_panel_1.createParallelGroup(Alignment.LEADING)
								.addGroup(gl_panel_1.createSequentialGroup()
									.addComponent(lbDtNascEdit, GroupLayout.PREFERRED_SIZE, 65, GroupLayout.PREFERRED_SIZE)
									.addPreferredGap(ComponentPlacement.RELATED)
									.addComponent(tfDtNascEdit, GroupLayout.PREFERRED_SIZE, 195, GroupLayout.PREFERRED_SIZE))
								.addGroup(gl_panel_1.createSequentialGroup()
									.addComponent(lbSexoEdit, GroupLayout.PREFERRED_SIZE, 65, GroupLayout.PREFERRED_SIZE)
									.addPreferredGap(ComponentPlacement.UNRELATED)
									.addComponent(rbtMascEdit)
									.addPreferredGap(ComponentPlacement.UNRELATED)
									.addComponent(rbtFemiEdit, GroupLayout.PREFERRED_SIZE, 71, GroupLayout.PREFERRED_SIZE)))))
					.addContainerGap())
		);
		gl_panel_1.setVerticalGroup(
			gl_panel_1.createParallelGroup(Alignment.TRAILING)
				.addGroup(Alignment.LEADING, gl_panel_1.createSequentialGroup()
					.addContainerGap()
					.addGroup(gl_panel_1.createParallelGroup(Alignment.TRAILING)
						.addComponent(btnSalvar)
						.addGroup(Alignment.LEADING, gl_panel_1.createSequentialGroup()
							.addGroup(gl_panel_1.createParallelGroup(Alignment.TRAILING)
								.addGroup(gl_panel_1.createSequentialGroup()
									.addGroup(gl_panel_1.createParallelGroup(Alignment.BASELINE)
										.addComponent(lbSexoEdit)
										.addComponent(rbtMascEdit)
										.addComponent(rbtFemiEdit))
									.addGap(4)
									.addGroup(gl_panel_1.createParallelGroup(Alignment.BASELINE)
										.addComponent(lbDtNascEdit)
										.addComponent(tfDtNascEdit, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
								.addGroup(gl_panel_1.createSequentialGroup()
									.addGroup(gl_panel_1.createParallelGroup(Alignment.LEADING)
										.addComponent(lbId, GroupLayout.PREFERRED_SIZE, 23, GroupLayout.PREFERRED_SIZE)
										.addComponent(lbIDvalue, GroupLayout.PREFERRED_SIZE, 23, GroupLayout.PREFERRED_SIZE))
									.addPreferredGap(ComponentPlacement.RELATED)
									.addGroup(gl_panel_1.createParallelGroup(Alignment.BASELINE)
										.addComponent(lbNomeEdit)
										.addComponent(tfNomeEdit, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))))
							.addGap(4)
							.addGroup(gl_panel_1.createParallelGroup(Alignment.BASELINE)
								.addComponent(lbCPFEdit)
								.addComponent(tfCpfEdit, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(panelEndEdit, GroupLayout.PREFERRED_SIZE, 125, Short.MAX_VALUE)))
					.addContainerGap(11, Short.MAX_VALUE))
		);
		gl_panel_1.linkSize(SwingConstants.VERTICAL, new Component[] {tfNomeEdit, lbNomeEdit, lbCPFEdit, tfCpfEdit, lbSexoEdit, tfDtNascEdit, btnSalvar});
		gl_panel_1.linkSize(SwingConstants.HORIZONTAL, new Component[] {tfNomeEdit, tfCpfEdit});
		
		JLabel lbLogradouro = new JLabel("Logradouro:");
		
		tfLograEdit = new JTextField();
		tfLograEdit.setColumns(10);
		
		tfNumEdit = new JTextField();
		tfNumEdit.setColumns(10);
		
		JLabel lblNumeroedit = new JLabel("Numero:");
		
		tfBairroEdit = new JTextField();
		tfBairroEdit.setColumns(10);
		
		JLabel lbBairroEdit = new JLabel("Bairro:");
		
		tfCidadeEdit = new JTextField();
		tfCidadeEdit.setColumns(10);
		
		JLabel lbCidadeEdit = new JLabel("Cidade:");
		
		JLabel lbCepEdit = new JLabel("CEP:");
		
		tfCepEdit = new JTextField();
		tfCepEdit.setColumns(10);
		
		comboUFEdit = new JComboBox();
		try {
			String[] listaEstados = convertEstados(estadoBO.listEstados());
			comboUFEdit.setModel(new DefaultComboBoxModel(listaEstados));
		} catch (Exception e) {
			MensagensUtil.addMsg(Main.this, "Erro ao buscar lista de estados!");
		}
		
		JLabel lbUFEdit = new JLabel("UF:");
		GroupLayout gl_panelEndEdit = new GroupLayout(panelEndEdit);
		gl_panelEndEdit.setHorizontalGroup(
			gl_panelEndEdit.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panelEndEdit.createSequentialGroup()
					.addContainerGap()
					.addGroup(gl_panelEndEdit.createParallelGroup(Alignment.LEADING)
						.addComponent(lbCidadeEdit, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
						.addComponent(lbBairroEdit, GroupLayout.PREFERRED_SIZE, 61, GroupLayout.PREFERRED_SIZE)
						.addComponent(lbLogradouro, GroupLayout.PREFERRED_SIZE, 70, GroupLayout.PREFERRED_SIZE))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(gl_panelEndEdit.createParallelGroup(Alignment.LEADING)
						.addComponent(tfBairroEdit, 214, 214, 214)
						.addComponent(tfLograEdit, 214, 214, 214)
						.addComponent(tfCidadeEdit, GroupLayout.PREFERRED_SIZE, 204, GroupLayout.PREFERRED_SIZE))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(gl_panelEndEdit.createParallelGroup(Alignment.LEADING)
						.addComponent(lbCepEdit, GroupLayout.PREFERRED_SIZE, 55, GroupLayout.PREFERRED_SIZE)
						.addComponent(lblNumeroedit, GroupLayout.PREFERRED_SIZE, 55, GroupLayout.PREFERRED_SIZE)
						.addComponent(lbUFEdit, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(gl_panelEndEdit.createParallelGroup(Alignment.LEADING)
						.addComponent(tfCepEdit, GroupLayout.DEFAULT_SIZE, 153, Short.MAX_VALUE)
						.addComponent(tfNumEdit, GroupLayout.DEFAULT_SIZE, 153, Short.MAX_VALUE)
						.addComponent(comboUFEdit, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addContainerGap())
		);
		gl_panelEndEdit.setVerticalGroup(
			gl_panelEndEdit.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panelEndEdit.createSequentialGroup()
					.addGap(3)
					.addGroup(gl_panelEndEdit.createParallelGroup(Alignment.BASELINE)
						.addComponent(lbLogradouro)
						.addComponent(tfLograEdit, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(lblNumeroedit)
						.addComponent(tfNumEdit, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(gl_panelEndEdit.createParallelGroup(Alignment.BASELINE)
						.addComponent(lbBairroEdit)
						.addComponent(tfBairroEdit, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(lbCepEdit)
						.addComponent(tfCepEdit, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(gl_panelEndEdit.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_panelEndEdit.createParallelGroup(Alignment.BASELINE)
							.addComponent(lbCidadeEdit)
							.addComponent(tfCidadeEdit, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
						.addGroup(gl_panelEndEdit.createParallelGroup(Alignment.BASELINE)
							.addComponent(lbUFEdit)
							.addComponent(comboUFEdit, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
					.addGap(17))
		);
		gl_panelEndEdit.linkSize(SwingConstants.HORIZONTAL, new Component[] {lbLogradouro, lbBairroEdit, lbCidadeEdit});
		gl_panelEndEdit.linkSize(SwingConstants.HORIZONTAL, new Component[] {tfNumEdit, tfCepEdit, comboUFEdit});
		gl_panelEndEdit.linkSize(SwingConstants.HORIZONTAL, new Component[] {tfLograEdit, tfBairroEdit, tfCidadeEdit});
		gl_panelEndEdit.linkSize(SwingConstants.HORIZONTAL, new Component[] {lblNumeroedit, lbCepEdit, lbUFEdit});
		panelEndEdit.setLayout(gl_panelEndEdit);
		panel_1.setLayout(gl_panel_1);
		internalFrame.getContentPane().setLayout(groupLayout);
		
		try {
			String[][] lista = pessoaBO.listagem(idsPessoas);
			tableListagem = new JTable();
			scrollPane.setViewportView(tableListagem);
			tableListagem.setModel(new DefaultTableModel(
				lista,
				new String[] {
					"Id", "Nome", "CPF", "Sexo", "Dt. Nasc.", "Logradouro", "CEP", "UF", "", ""
				}
			));
			
			Action actionDelecao = new AbstractAction() {
				private static final long serialVersionUID = 1L;

				@Override
				public void actionPerformed(ActionEvent e) {
					int resp = JOptionPane.showConfirmDialog(Main.this, "Deseja apagar esse registro?");
					if (resp == 0) {
						JTable table = (JTable) e.getSource();
						
						int line = Integer.parseInt(e.getActionCommand());
						((DefaultTableModel) table.getModel()).removeRow(line);
						try {
							pessoaDTO = pessoaBO.buscaPorId(idsPessoas.get(line));
							pessoaBO.remover(idsPessoas.get(line), getPessoaDTO().getEnderecoDTO().getId());
							idsPessoas.remove(line);
							MensagensUtil.addMsg(Main.this, "Cadastro removido com sucesso!");
						} catch (NegocioException e1) {
							MensagensUtil.addMsg(Main.this, e1.getMessage());
						}
					}
				}
			};
			
			Action actionEdicao = new AbstractAction() {
				private static final long serialVersionUID = 1L;

				@Override
				public void actionPerformed(ActionEvent e) {
					int line = Integer.parseInt(e.getActionCommand());
					try {
						pessoaDTO = pessoaBO.buscaPorId(idsPessoas.get(line));
						fillInternalFrame(pessoaDTO);
						internalFrame.setVisible(true);
					} catch (NegocioException e1) {
						MensagensUtil.addMsg(Main.this, e1.getMessage());
					}
				}
			};
			
			ButtonColumn btEdicao = new ButtonColumn(tableListagem, actionEdicao, 8);
			ButtonColumn btDelecao = new ButtonColumn(tableListagem, actionDelecao, 9);
			btEdicao.setMnemonic(KeyEvent.VK_E);
			btDelecao.setMnemonic(KeyEvent.VK_D);
		} catch (Exception e) {
			MensagensUtil.addMsg(Main.this, e.getMessage());
		}
		panelListagem.setLayout(gl_panelListagem);
		
		JPanel panelConsulta = new JPanel();
		mainTabbedPane.addTab("Consulta", null, panelConsulta, null);
		
		JPanel panel = new JPanel();
		panel.setBorder(new TitledBorder(null, "Listagem", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		
		JButton btConsultar = new JButton("Consultar");
		
		JPanel panelConsultaFiltro = new JPanel();
		panelConsultaFiltro.setBorder(new TitledBorder(null, "Filtro", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		
		GroupLayout gl_panelConsulta = new GroupLayout(panelConsulta);
		gl_panelConsulta.setHorizontalGroup(
			gl_panelConsulta.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panelConsulta.createSequentialGroup()
					.addContainerGap()
					.addGroup(gl_panelConsulta.createParallelGroup(Alignment.LEADING)
						.addComponent(btConsultar)
						.addComponent(panelConsultaFiltro, GroupLayout.DEFAULT_SIZE, 1089, Short.MAX_VALUE)
						.addComponent(panel, GroupLayout.PREFERRED_SIZE, 718, GroupLayout.PREFERRED_SIZE))
					.addContainerGap())
		);
		gl_panelConsulta.setVerticalGroup(
			gl_panelConsulta.createParallelGroup(Alignment.TRAILING)
				.addGroup(Alignment.LEADING, gl_panelConsulta.createSequentialGroup()
					.addContainerGap()
					.addComponent(panelConsultaFiltro, GroupLayout.PREFERRED_SIZE, 160, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(btConsultar)
					.addPreferredGap(ComponentPlacement.RELATED, 120, Short.MAX_VALUE)
					.addComponent(panel, GroupLayout.PREFERRED_SIZE, 146, GroupLayout.PREFERRED_SIZE)
					.addContainerGap())
		);
		
		JTextField tfNomeConsulta = new JTextField();
		tfNomeConsulta.setColumns(10);
		
		JLabel lblNomeConsulta = new JLabel("Nome:");
		
		tfCpfConsulta = new JTextField();
		tfCpfConsulta.setColumns(10);
		
		JLabel lblCpf = new JLabel("Cpf:");
		
		JRadioButton rdbtnMascConsulta = new JRadioButton("Masculino");
		buttonGroup.add(rdbtnMascConsulta);
		
		JLabel lblSexoConsulta = new JLabel("Sexo:");
		
		JRadioButton rdbtnFemiConsulta = new JRadioButton("Feminino");
		buttonGroup.add(rdbtnFemiConsulta);
		
		JLabel lblOrderBy = new JLabel("Order By:");
		
		JRadioButton rbtNomeOrder = new JRadioButton("Nome");
		rbtNomeOrder.setSelected(true);
		btgOrderBy.add(rbtNomeOrder);
		
		JRadioButton rbtCpfOrder = new JRadioButton("CPF");
		btgOrderBy.add(rbtCpfOrder);
		
		JLabel lbCepConsulta = new JLabel("CEP:");
		
		tfCepConsulta = new JTextField();
		tfCepConsulta.setColumns(10);
		
		JComboBox comboUfConsulta = new JComboBox();
		try {
			String[] listaEstados = convertEstados(estadoBO.listEstados());
			comboUfConsulta.setModel(new DefaultComboBoxModel(listaEstados));
		} catch (Exception e) {
			MensagensUtil.addMsg(Main.this, "Erro ao buscar lista de estados!");
		}
		
		JLabel lbUfConsulta = new JLabel("UF:");
		GroupLayout gl_panelConsultaFiltro = new GroupLayout(panelConsultaFiltro);
		gl_panelConsultaFiltro.setHorizontalGroup(
			gl_panelConsultaFiltro.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panelConsultaFiltro.createSequentialGroup()
					.addGroup(gl_panelConsultaFiltro.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_panelConsultaFiltro.createSequentialGroup()
							.addGap(71)
							.addComponent(tfNomeConsulta, GroupLayout.PREFERRED_SIZE, 225, GroupLayout.PREFERRED_SIZE))
						.addGroup(gl_panelConsultaFiltro.createSequentialGroup()
							.addGap(71)
							.addComponent(tfCpfConsulta, GroupLayout.PREFERRED_SIZE, 218, GroupLayout.PREFERRED_SIZE))
						.addGroup(gl_panelConsultaFiltro.createSequentialGroup()
							.addContainerGap()
							.addGroup(gl_panelConsultaFiltro.createParallelGroup(Alignment.TRAILING, false)
								.addComponent(lblSexoConsulta, Alignment.LEADING, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
								.addComponent(lblOrderBy, Alignment.LEADING, GroupLayout.DEFAULT_SIZE, 59, Short.MAX_VALUE)
								.addComponent(lblCpf, Alignment.LEADING, GroupLayout.PREFERRED_SIZE, 33, GroupLayout.PREFERRED_SIZE)
								.addComponent(lblNomeConsulta, Alignment.LEADING, GroupLayout.PREFERRED_SIZE, 51, GroupLayout.PREFERRED_SIZE))
							.addPreferredGap(ComponentPlacement.RELATED)
							.addGroup(gl_panelConsultaFiltro.createParallelGroup(Alignment.LEADING)
								.addGroup(gl_panelConsultaFiltro.createSequentialGroup()
									.addComponent(rdbtnMascConsulta, GroupLayout.PREFERRED_SIZE, 94, GroupLayout.PREFERRED_SIZE)
									.addGap(18)
									.addComponent(rdbtnFemiConsulta, GroupLayout.PREFERRED_SIZE, 91, GroupLayout.PREFERRED_SIZE))
								.addGroup(gl_panelConsultaFiltro.createSequentialGroup()
									.addComponent(rbtNomeOrder, GroupLayout.PREFERRED_SIZE, 83, GroupLayout.PREFERRED_SIZE)
									.addGap(29)
									.addComponent(rbtCpfOrder, GroupLayout.PREFERRED_SIZE, 75, GroupLayout.PREFERRED_SIZE)))))
					.addPreferredGap(ComponentPlacement.UNRELATED, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
					.addGroup(gl_panelConsultaFiltro.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_panelConsultaFiltro.createSequentialGroup()
							.addComponent(lbCepConsulta, GroupLayout.PREFERRED_SIZE, 54, GroupLayout.PREFERRED_SIZE)
							.addGap(4)
							.addComponent(tfCepConsulta, GroupLayout.PREFERRED_SIZE, 203, GroupLayout.PREFERRED_SIZE))
						.addGroup(gl_panelConsultaFiltro.createSequentialGroup()
							.addComponent(lbUfConsulta, GroupLayout.PREFERRED_SIZE, 54, GroupLayout.PREFERRED_SIZE)
							.addGap(4)
							.addComponent(comboUfConsulta, GroupLayout.PREFERRED_SIZE, 203, GroupLayout.PREFERRED_SIZE)))
					.addGap(390))
		);
		gl_panelConsultaFiltro.setVerticalGroup(
			gl_panelConsultaFiltro.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panelConsultaFiltro.createSequentialGroup()
					.addGroup(gl_panelConsultaFiltro.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_panelConsultaFiltro.createSequentialGroup()
							.addGroup(gl_panelConsultaFiltro.createParallelGroup(Alignment.BASELINE)
								.addComponent(lblNomeConsulta)
								.addComponent(tfNomeConsulta, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
							.addGap(9)
							.addGroup(gl_panelConsultaFiltro.createParallelGroup(Alignment.BASELINE)
								.addComponent(lblCpf)
								.addComponent(tfCpfConsulta, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
						.addGroup(gl_panelConsultaFiltro.createSequentialGroup()
							.addGroup(gl_panelConsultaFiltro.createParallelGroup(Alignment.LEADING)
								.addGroup(gl_panelConsultaFiltro.createSequentialGroup()
									.addGap(3)
									.addComponent(lbCepConsulta))
								.addComponent(tfCepConsulta, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
							.addGap(6)
							.addGroup(gl_panelConsultaFiltro.createParallelGroup(Alignment.LEADING)
								.addGroup(gl_panelConsultaFiltro.createSequentialGroup()
									.addGap(3)
									.addComponent(lbUfConsulta))
								.addComponent(comboUfConsulta, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))))
					.addGap(6)
					.addGroup(gl_panelConsultaFiltro.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblSexoConsulta)
						.addComponent(rdbtnMascConsulta)
						.addComponent(rdbtnFemiConsulta))
					.addGap(29)
					.addGroup(gl_panelConsultaFiltro.createParallelGroup(Alignment.TRAILING)
						.addComponent(lblOrderBy)
						.addComponent(rbtCpfOrder)
						.addComponent(rbtNomeOrder))
					.addContainerGap())
		);
		gl_panelConsultaFiltro.linkSize(SwingConstants.VERTICAL, new Component[] {lblNomeConsulta, lblCpf, lblOrderBy});
		gl_panelConsultaFiltro.linkSize(SwingConstants.VERTICAL, new Component[] {tfNomeConsulta, tfCpfConsulta});
		gl_panelConsultaFiltro.linkSize(SwingConstants.HORIZONTAL, new Component[] {tfNomeConsulta, tfCpfConsulta});
		gl_panelConsultaFiltro.linkSize(SwingConstants.HORIZONTAL, new Component[] {lblNomeConsulta, lblCpf});
		panelConsultaFiltro.setLayout(gl_panelConsultaFiltro);
		
		JScrollPane panelListagemConsulta = new JScrollPane();
		GroupLayout gl_panel = new GroupLayout(panel);
		gl_panel.setHorizontalGroup(
			gl_panel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel.createSequentialGroup()
					.addComponent(panelListagemConsulta, GroupLayout.PREFERRED_SIZE, 704, GroupLayout.PREFERRED_SIZE)
					.addContainerGap(253, Short.MAX_VALUE))
		);
		gl_panel.setVerticalGroup(
			gl_panel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel.createSequentialGroup()
					.addComponent(panelListagemConsulta, GroupLayout.PREFERRED_SIZE, 123, GroupLayout.PREFERRED_SIZE)
					.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
		);
		
		tableConsulta = new JTable();
		tableConsulta.setEnabled(false);
		valuesConsulta = new DefaultTableModel(
				new String[][] {},
				new String[] {
					"Id", "Nome", "CPF", "Sexo", "Dt. Nasc.", "Logradouro", "CEP", "UF"
				}
		);
		tableConsulta.setModel(valuesConsulta);
		panelListagemConsulta.setViewportView(tableConsulta);
		panel.setLayout(gl_panel);
		panelConsulta.setLayout(gl_panelConsulta);
		contentPane.setLayout(gl_contentPane);
		
		btConsultar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String nome = tfNomeConsulta.getText();
				Long cpf = tfCpfConsulta.getText().equals("") ? null : Long.parseLong(tfCpfConsulta.getText());
				char sexo = rdbtnMascConsulta.isSelected() ? 'M' : 'F';
				String orderBy = rbtNomeOrder.isSelected() ? "nome" : "cpf";
				
				PessoaBO pessoaBOConsulta = new PessoaBO();
				try {
					String[][] valuesConsulta = pessoaBOConsulta.listaConsulta(nome, cpf, sexo, orderBy);
					setValuesConsulta(valuesConsulta);
				} catch (Exception e) {
					MensagensUtil.addMsg(Main.this, e.getMessage());
				}
			}
		});
	}
	
	public PessoaDTO getPessoaDTO() {
		return pessoaDTO;
	}

	private void setValuesConsulta(String[][] values) {
		valuesConsulta.setDataVector(values, new String[] {
				"Id", "Nome", "CPF", "Sexo", "Dt. Nasc.", "Logradouro", "CEP", "UF"
			});
	}
	
	private void fillInternalFrame(PessoaDTO pessoaDTO) {
		this.lbIDvalue.setText(pessoaDTO.getId().toString());
		this.tfNomeEdit.setText(pessoaDTO.getNome());
		this.tfCpfEdit.setText(pessoaDTO.getCpf().toString());
		this.tfDtNascEdit.setText(dateFormat.format(pessoaDTO.getDt_nasc()));
		this.rbtMascEdit.setSelected(pessoaDTO.getSexo() == 'M');
		this.rbtFemiEdit.setSelected(pessoaDTO.getSexo() == 'F');
		
		EnderecoDTO enderecoDTO = pessoaDTO.getEnderecoDTO();
		this.tfLograEdit.setText(enderecoDTO.getLogradouro());
		this.tfBairroEdit.setText(enderecoDTO.getBairro());
		this.tfNumEdit.setText(enderecoDTO.getNumero().toString());
		this.tfCidadeEdit.setText(enderecoDTO.getCidade());
		this.tfCepEdit.setText(enderecoDTO.getCep().toString());
		this.comboUFEdit.setSelectedIndex(enderecoDTO.getEstadoDTO().getId() - 1);
	}
	
	private String[] convertEstados(List<EstadoDTO> lista) {
		String[] result = new String[lista.size()];
		for (int i = 0; i < lista.size(); i++) {
			EstadoDTO estadoDTO = lista.get(i);
			result[i] = estadoDTO.getDescricao();
		}
		return result;
	}
}

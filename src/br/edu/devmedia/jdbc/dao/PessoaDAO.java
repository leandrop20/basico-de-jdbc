package br.edu.devmedia.jdbc.dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import com.mysql.jdbc.Statement;

import br.edu.devmedia.jdbc.ConexaoUtil;
import br.edu.devmedia.jdbc.Exception.PersistenciaException;
import br.edu.devmedia.jdbc.dto.EnderecoDTO;
import br.edu.devmedia.jdbc.dto.EstadoDTO;
import br.edu.devmedia.jdbc.dto.PessoaDTO;

public class PessoaDAO implements GenericoDAO<PessoaDTO> {

	@Override
	public void inserir(PessoaDTO pessoaDTO) throws PersistenciaException {
		try {
			int keyEndereco = insereEndereco(pessoaDTO.getEnderecoDTO());
			Connection connection = ConexaoUtil.getInstance().getConnection();
			
			String sql = "INSERT INTO pessoas (nome, cpf, sexo, dt_nasc, address_id) VALUES (?, ?, ?, ?, ?)";
			
			PreparedStatement statement = connection.prepareStatement(sql);
			statement.setString(1, pessoaDTO.getNome());
			statement.setLong(2, pessoaDTO.getCpf());
			statement.setString(3, String.valueOf(pessoaDTO.getSexo()));
			statement.setDate(4, new Date(pessoaDTO.getDt_nasc().getTime()));
			statement.setInt(5, keyEndereco);
			
			statement.execute();
			connection.close();
		} catch (Exception e) {
			e.printStackTrace();
			throw new PersistenciaException(e.getMessage(), e);
		}
	}
	
	private int insereEndereco(EnderecoDTO enderecoDTO) throws PersistenciaException {
		int key = 0;
		try {
			Connection connection = ConexaoUtil.getInstance().getConnection();
			
			String sql = "INSERT INTO address (logradouro, bairro, cidade, numero, cep, estados_id) VALUES (?, ?, ?, ?, ?, ?)";
			
			PreparedStatement statement = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
			statement.setString(1, enderecoDTO.getLogradouro());
			statement.setString(2, enderecoDTO.getBairro());
			statement.setString(3, enderecoDTO.getCidade());
			statement.setLong(4, enderecoDTO.getNumero());
			statement.setInt(5, enderecoDTO.getCep());
			statement.setInt(6, enderecoDTO.getEstadoDTO().getId());
			
			statement.execute();
			ResultSet result = statement.getGeneratedKeys();
			if (result.next()) {
				key = result.getInt(1);
			}
			connection.close();
		} catch (Exception e) {
			e.printStackTrace();
			throw new PersistenciaException(e.getMessage(), e);
		}
		return key;
	}

	@Override
	public void atualizar(PessoaDTO pessoaDTO) throws PersistenciaException {
		try {
			Connection connection = ConexaoUtil.getInstance().getConnection();
			
			String sql = "UPDATE pessoas SET nome = ?, cpf = ?, sexo = ?, dt_nasc = ? WHERE id = ?";
			
			PreparedStatement statement = connection.prepareStatement(sql);
			statement.setString(1, pessoaDTO.getNome());
			statement.setLong(2, pessoaDTO.getCpf());
			statement.setString(3, String.valueOf(pessoaDTO.getSexo()));
			statement.setDate(4, new Date(pessoaDTO.getDt_nasc().getTime()));
			statement.setInt(5, pessoaDTO.getId());
			
			statement.execute();
			connection.close();
			atualizaEndereco(pessoaDTO.getEnderecoDTO());
		} catch (Exception e) {
			e.printStackTrace();
			throw new PersistenciaException(e.getMessage(), e);
		}
	}
	
	private void atualizaEndereco(EnderecoDTO enderecoDTO) throws PersistenciaException {
		try {
			Connection connection = ConexaoUtil.getInstance().getConnection();
			
			String sql = "UPDATE address SET logradouro = ?, bairro = ?, cidade = ?, numero = ?, cep = ?, estados_id = ? WHERE id = ?";
			
			PreparedStatement statement = connection.prepareStatement(sql);
			statement.setString(1, enderecoDTO.getLogradouro());
			statement.setString(2, enderecoDTO.getBairro());
			statement.setString(3, enderecoDTO.getCidade());
			statement.setLong(4, enderecoDTO.getNumero());
			statement.setInt(5, enderecoDTO.getCep());
			statement.setInt(6, enderecoDTO.getEstadoDTO().getId());
			statement.setInt(7, enderecoDTO.getId());
			
			statement.execute();
			connection.close();
		} catch (Exception e) {
			e.printStackTrace();
			throw new PersistenciaException(e.getMessage(), e);
		}
	}

	@Override
	public void remover(Integer id) throws PersistenciaException {
		try {
			Connection connection = ConexaoUtil.getInstance().getConnection();
			
			String sql = "DELETE FROM pessoas WHERE id = ?";
			
			PreparedStatement statement = connection.prepareStatement(sql);
			statement.setInt(1, id);
			
			statement.execute();
			connection.close();
		} catch (Exception e) {
			e.printStackTrace();
			throw new PersistenciaException(e.getMessage(), e);
		}
	}
	
	public void removerEndereco(Integer id) throws PersistenciaException {
		try {
			Connection connection = ConexaoUtil.getInstance().getConnection();
			
			String sql = "DELETE FROM address WHERE id = ?";
			
			PreparedStatement statement = connection.prepareStatement(sql);
			statement.setInt(1, id);
			
			statement.execute();
			connection.close();
		} catch (Exception e) {
			e.printStackTrace();
			throw new PersistenciaException(e.getMessage(), e);
		}
	}
	
	public boolean removeTudo() {
		return true;
	}

	@Override
	public List<PessoaDTO> listarTodos() throws PersistenciaException {
		List<PessoaDTO> listaPessoas = new ArrayList<PessoaDTO>();
		try {
			Connection connection = ConexaoUtil.getInstance().getConnection();
			
			String sql = "SELECT * FROM pessoas";
			
			PreparedStatement statement = connection.prepareStatement(sql);
			ResultSet resultSet = statement.executeQuery();
			
			while(resultSet.next()) {
				PessoaDTO dto = new PessoaDTO();
				dto.setId(resultSet.getInt("id"));
				dto.setNome(resultSet.getString("nome"));
				dto.setCpf(resultSet.getLong("cpf"));
				dto.setSexo(resultSet.getString("sexo").charAt(0));
				dto.setDt_nasc(resultSet.getDate("dt_nasc"));
				dto.setEnderecoDTO(buscaEnderecoPorID(resultSet.getInt("address_id")));
				
				listaPessoas.add(dto);
			}
			connection.close();
		} catch (Exception e) {
			e.printStackTrace();
			throw new PersistenciaException(e.getMessage(), e);
		}
		return listaPessoas;
	}
	
	private EnderecoDTO buscaEnderecoPorID(Integer id) throws PersistenciaException {
		EnderecoDTO enderecoDTO = null;
		try {
			Connection connection = ConexaoUtil.getInstance().getConnection();
			
			String sql = "SELECT * FROM address WHERE id = ?";
			
			PreparedStatement statement = connection.prepareStatement(sql);
			statement.setInt(1, id);
			
			ResultSet result = statement.executeQuery();
			
			if (result.next()) {
				enderecoDTO = new EnderecoDTO();
				enderecoDTO.setId(result.getInt(1));
				enderecoDTO.setLogradouro(result.getString(2));
				enderecoDTO.setBairro(result.getString(3));
				enderecoDTO.setCidade(result.getString(4));
				enderecoDTO.setNumero(result.getLong(5));
				enderecoDTO.setCep(result.getInt(6));
				enderecoDTO.setEstadoDTO(buscaEstadoPorID(result.getInt(7)));
			}
			connection.close();
		} catch (Exception e) {
			e.printStackTrace();
			throw new PersistenciaException(e.getMessage(), e);
		}
		return enderecoDTO;
	}
	
	private EstadoDTO buscaEstadoPorID(Integer id) throws PersistenciaException {
		EstadoDTO estadoDTO = null;
		try {
			Connection connection = ConexaoUtil.getInstance().getConnection();
			
			String sql = "SELECT * FROM estados WHERE id = ?";
			
			PreparedStatement statement = connection.prepareStatement(sql);
			statement.setInt(1, id);
			
			ResultSet result = statement.executeQuery();
			
			if (result.next()) {
				estadoDTO = new EstadoDTO();
				estadoDTO.setId(result.getInt(1));
				estadoDTO.setUf(result.getString(2));
				estadoDTO.setDescricao(result.getString(3));
			}
			
			connection.close();
		} catch (Exception e) {
			e.printStackTrace();
			throw new PersistenciaException(e.getMessage(), e);
		}
		return estadoDTO;
	}

	@Override
	public PessoaDTO buscarPorId(Integer id) throws PersistenciaException {
		PessoaDTO dto = null;
		try {
			Connection connection = ConexaoUtil.getInstance().getConnection();
			
			String sql = "SELECT * FROM pessoas WHERE id = ?";
			
			PreparedStatement statement = connection.prepareStatement(sql);
			statement.setInt(1, id);
			
			ResultSet resultSet = statement.executeQuery();
			if (resultSet.next()) {
				dto = new PessoaDTO();
				dto.setId(resultSet.getInt("id"));
				dto.setNome(resultSet.getString("nome"));
				dto.setCpf(resultSet.getLong("cpf"));
				dto.setSexo(resultSet.getString("sexo").charAt(0));
				dto.setDt_nasc(resultSet.getDate("dt_nasc"));
				dto.setEnderecoDTO(buscaEnderecoPorID(resultSet.getInt("address_id")));
			}
			connection.close();
		} catch (Exception e) {
			e.printStackTrace();
			throw new PersistenciaException(e.getMessage(), e);
		}
		return dto;
	}
	
	public List<PessoaDTO> filtrarPessoa(String nome, Long cpf, String sexo, String orderBy) throws PersistenciaException {
		List<PessoaDTO> lista = new ArrayList<PessoaDTO>();
		try {
			Connection connection = ConexaoUtil.getInstance().getConnection();
			
			String sql = "SELECT * FROM pessoas ";
			boolean ultimo = false;
			if (nome != null && !nome.equals("")) {
				sql += "WHERE NOME LIKE ? ";
				ultimo = true;
			}
			if (cpf != null && !cpf.equals("")) {
				if (ultimo) {
					sql += " AND ";
				} else {
					ultimo = true;
					sql += " WHERE ";
				}
				sql += " CPF LIKE ? ";
			}
			if (sexo != null && !sexo.equals("")) {
				if (ultimo) {
					sql += " AND ";
				} else {
					sql += " WHERE ";
				}
				sql += " SEXO = ? ";
			}
			sql += " ORDER BY " + orderBy;
			
			PreparedStatement statement = connection.prepareStatement(sql);
			int cont = 0;
			if (nome != null && !nome.equals("")) {
				statement.setString(++cont, "%" + nome + "%");
			}
			if (cpf != null && !cpf.equals("")) {
				statement.setString(++cont, "%" + cpf + "%");
			}
			if (sexo != null && !sexo.equals("")) {
				statement.setString(++cont, sexo);
			}
			
			ResultSet resultSet = statement.executeQuery();
			while (resultSet.next()) {
				PessoaDTO dto = new PessoaDTO();
				dto.setId(resultSet.getInt("id"));
				dto.setNome(resultSet.getString("nome"));
				dto.setCpf(resultSet.getLong("cpf"));
				dto.setSexo(resultSet.getString("sexo").charAt(0));
				dto.setDt_nasc(resultSet.getDate("dt_nasc"));
				lista.add(dto);
			}
			connection.close();
		} catch (Exception e) {
			e.printStackTrace();
			throw new PersistenciaException(e.getMessage(), e);
		}
		return lista;
	}
	
}
